#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <pthread.h>
#include <complex.h>

double *v;
double  *real, *imaginar;
int n, numThreads;

// alocare memorie pentru vectorii de input si output
void init()
{
	v = calloc(n + 1, sizeof(double));
	real = calloc(n + 1, sizeof(double));
	imaginar = calloc(n + 1, sizeof(double));
	if(v == NULL || real == NULL || imaginar == NULL) {
		printf("malloc failed!");
		exit(1);
	}
}

void* threadFunction(void *args){
	int thread_id = *(int *) args;
	// calculez indicii de inceput si final pentru fiecare thread
	int start = thread_id * ceil((double) n/numThreads);
	int end = fmin(n, (thread_id+1)* (ceil((double) n/numThreads)));

	for(int i = start; i < end; i++){
		for(int j = 0; j < n; j++){
			double unghi = 2 * M_PI * j * i / n; 
			// pentru partea reala, folosesc cosinus
			real[i] += v[j] * cos(unghi);
			// pentru partea imaginara, sinus
			imaginar[i] -= v[j] * sin(unghi);
		}
	}
	return NULL;
}

int main(int argc, char * argv[]) {
	FILE *fin = fopen(argv[1], "r");
	FILE *fout = fopen(argv[2], "w");
	numThreads = atoi(argv[3]);
	int i = 0;
	float aux;
	
	// citesc nr n de elemente din vector
	if(fscanf(fin, "%d", &n) == -1)
		exit(1);		
	init();
	
	
	// citirea elementelor din vector
	while(fscanf(fin, "%f\n", &aux) != -1){
		v[i] = aux;
		i++;
	}
	
	pthread_t tid[numThreads];	
	int thread_id[numThreads];
	for(i = 0;i < numThreads; i++)
		thread_id[i] = i;
	
	// creez thread-urile
	for(i = 0; i < numThreads; i++) {
		pthread_create(&(tid[i]), NULL, threadFunction, &(thread_id[i]));
	}

	for(i = 0; i < numThreads; i++) {
		pthread_join(tid[i], NULL);
	}
	
	// scriu rezultele in fisierul de output
	fprintf(fout, "%d\n", n);
	for(int i = 0; i < n; i++){
		fprintf(fout, "%f ", real[i]);
		fprintf(fout,"%f\n", imaginar[i]);
	}
	fclose(fin);
	fclose(fout);
}
