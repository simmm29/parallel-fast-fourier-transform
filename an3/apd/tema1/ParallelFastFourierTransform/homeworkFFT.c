#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <pthread.h>
#include <complex.h>

int n, numThreads;
typedef double complex cplx;
cplx *buf, *out;

// alocare memorie pentru vectorii de input si output
void init()
{
	buf = calloc(n + 1, sizeof(cplx));
	out = calloc(n + 1, sizeof(cplx));
	if(buf == NULL || out == NULL) {
		printf("malloc failed!");
		exit(1);
	}
}

void _fft(cplx buf[], cplx out[], int n, int step)
{
	if (step < n) {
		_fft(out, buf, n, step * 2);
		_fft(out + step, buf + step, n, step * 2);
		
		
		for (int i = 0; i < n; i += 2 * step) {
			cplx t = cexp(-I * M_PI * i / n) * out[i + step];
			buf[i / 2]     = out[i] + t;
			buf[(i + n)/2] = out[i] - t;
		}
		
	}
			
}

// cazul cu 2 thread-uri
void* threadFunction_2(void* args){
	int thread_id = *(int*)args;
	// daca este fiul stang
	if(thread_id == 0)
		_fft(out, buf, n, 2);
	else
		// daca este fiul drept
		_fft(out + 1, buf + 1, n, 2);
	return NULL;
}

// cazul cu 4 thread-uri
void *threadFunction_4(void* args){
	int thread_id = *(int *) args;
	// nepotul stang al fiului stang
	if(thread_id == 0)
		_fft(buf, out, n, 4);
	// nepotul drept al fiului stang
	if(thread_id == 1)
		_fft(buf + 2, out + 2, n, 4);
	// nepotul stang al fiului drept
	if(thread_id == 2)
		_fft(buf + 1, out + 1, n, 4);
	// nepotul drept al fiului drept
	if(thread_id == 3)
		_fft(buf + 3, out + 3, n, 4);
	return NULL;
}
	
int main(int argc, char * argv[]){
	
	FILE *fin = fopen(argv[1], "r");
	FILE *fout = fopen(argv[2], "w");
	numThreads = atoi(argv[3]);
	int i = 0;
	float aux;
	
	//citesc din fisier numarul n de elementele ale vectorului de input
	if(fscanf(fin, "%d", &n) == -1)
		exit(1); 
	init();

	// citirea elementelor din vector
	while(fscanf(fin, "%f\n", &aux) != -1){
		buf[i] = aux;
		i++;
	}

	pthread_t tid[numThreads];	
	int thread_id[numThreads];

	for(i = 0;i < numThreads; i++)
		thread_id[i] = i;

	for (int i = 0; i < n; i++)
			out[i] = buf[i];

	// cazul pentru 1 thread (secvential)
	if(numThreads == 1){
		_fft(buf, out, n, 1);
		fprintf(fout, "%d\n", n);
		for(int i = 0; i < n; i++){
			fprintf(fout, "%f ", creal(buf[i]));
			fprintf(fout,"%f\n", cimag(buf[i]));
		}
	}

	// cazul pentru 2 thread-uri
	if(numThreads == 2){
		// creez cele 2 thread-uri
		pthread_create(&(tid[0]), NULL, threadFunction_2, &(thread_id[0])); // pt fiul stang
		pthread_create(&(tid[1]), NULL, threadFunction_2, &(thread_id[1])); // pt fiul drept
			
		for(i = 0; i < numThreads; i++) {
			pthread_join(tid[i], NULL);
		}
		
		// combinarea solutiilor tatalui
		int step = 1;
		for (int i = 0; i < n; i += 2 * step) {
			cplx t = cexp(-I * M_PI * i / n) * out[i + step];
			buf[i / 2]     = out[i] + t;
			buf[(i + n)/2] = out[i] - t;
		}

		// scriu in fisier rezultatele
		fprintf(fout, "%d\n", n);
		for(int i = 0; i < n; i++){
			fprintf(fout, "%f ", creal(buf[i]));
			fprintf(fout,"%f\n", cimag(buf[i]));
		}
	}

	// cazul pt 4 thread-uri
	if(numThreads == 4){
		pthread_create(&(tid[0]), NULL, threadFunction_4, &(thread_id[0])); // nepot stang al primului fiu
		pthread_create(&(tid[1]), NULL, threadFunction_4, &(thread_id[1])); // nepot drept al primului fiu
		pthread_create(&(tid[2]), NULL, threadFunction_4, &(thread_id[2])); // nepot stang al celui de-al doilea fiu
		pthread_create(&(tid[3]), NULL, threadFunction_4, &(thread_id[3])); // nepot drept al celui de-al doilea fiu

		for(int i = 0; i < numThreads; i++)
			pthread_join(tid[i], NULL);

		// combin solutiile fiului stang
		int step = 2;
		for (int i = 0; i < n; i += 2 * step) {
				// out si buf sunt inversate, intrucat numai primul apel e de forma
				// _fft(buf, out, n, step)
				cplx t = cexp(-I * M_PI * i / n) * buf[i + step];
				out[i / 2]     = buf[i] + t;
				out[(i + n)/2] = buf[i] - t;
			}

		// combin solutiile fiului drept
		for (int i = 0; i < n; i += 2 * step) {
				// adaug 1, deoarece fiind fiul drept, apelul functiei e de forma 
				// _fft(out + STEP, buf + STEP, n, STEP * 2), unde step e 1 de la 
				// primul apel al functiei (bunicul)
				cplx t = cexp(-I * M_PI * i / n) * buf[i + step + 1]; 
				out[i / 2  + 1]     = buf[i + 1] + t; 
				out[(i + n)/2 + 1] = buf[i + 1] - t;
			}

		// combin solutiile bunicului
		step = 1;
		for (int i = 0; i < n; i += 2 * step) {
				cplx t = cexp(-I * M_PI * i / n) * out[i + step];
				buf[i / 2]     = out[i] + t;
				buf[(i + n)/2] = out[i] - t;
			}	

		fprintf(fout, "%d\n", n);
		for (int i = 0; i < n; i++){
			fprintf(fout, "%f ", creal(buf[i]));
			fprintf(fout, "%f\n", cimag(buf[i]));
		}
	}
	
	fclose(fin);
	fclose(fout);

}
























